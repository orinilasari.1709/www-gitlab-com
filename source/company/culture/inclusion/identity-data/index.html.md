---
layout: markdown_page
title: "Identity data"
---

#### GitLab Identity Data

Data as of 2019-10-31

##### Country Specific Data

| **Country Information**                     | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| Based in APAC                               | 73        | 7.33%           |
| Based in EMEA                               | 277       | 27.81%          |
| Based in LATAM                              | 13        | 1.31%           |
| Based in NORAM                              | 633       | 63.55%          |
| **Total Team Members**                      | **996**   | **100%**        |

##### Gender Data

| **Gender (All)**                            | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| Men                                         | 719       | 72.19%          |
| Women                                       | 277       | 27.81%          |
| Other Gender Identities                     | 0         | 0%              |
| **Total Team Members**                      | **996**   | **100%**        |

| **Gender in Leadership**                    | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| Men in Leadership                           | 45        | 78.95%          |
| Women in Leadership                         | 12        | 21.05%          |
| Other Gender Identities                     | 0         | 0%              |
| **Total Team Members**                      | **57**    | **100%**        |

| **Gender in Development**                   | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| Men in Development                          | 349       | 82.31%          |
| Women in Development                        | 75        | 17.69%          |
| Other Gender Identities                     | 0         | 0%              |
| **Total Team Members**                      | **424**   | **100%**        |

##### Race/Ethnicity Data

| **Race/Ethnicity (US Only)**                | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 1         | 0.17%           |
| Asian                                       | 35        | 5.83%           |
| Black or African American                   | 20        | 3.33%           |
| Hispanic or Latino                          | 33        | 5.50%           |
| Native Hawaiian or Other Pacific Islander   | 0         | 0.00%           |
| Two or More Races                           | 23        | 3.83%           |
| White                                       | 349       | 58.17%          |
| Unreported                                  | 139       | 23.17%          |
| **Total Team Members**                      | **600**   | **100%**        |

| **Race/Ethnicity in Development (US Only)** | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 0         | 0.00%           |
| Asian                                       | 13        | 7.65%           |
| Black or African American                   | 4         | 2.35%           |
| Hispanic or Latino                          | 10        | 5.88%           |
| Native Hawaiian or Other Pacific Islander   | 0         | 0.00%           |
| Two or More Races                           | 7         | 4.12%           |
| White                                       | 104       | 61.18%          |
| Unreported                                  | 32        | 18.82%          |
| **Total Team Members**                      | **170**   | **100%**        |

| **Race/Ethnicity in Leadership (US Only)**  | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 0         | 0.00%           |
| Asian                                       | 6         | 13.04%          |
| Black or African American                   | 0         | 0.00%           |
| Hispanic or Latino                          | 0         | 0.00%           |
| Native Hawaiian or Other Pacific Islander   | 0         | 0.00%           |
| Two or More Races                           | 2         | 4.35%           |
| White                                       | 27        | 58.70%          |
| Unreported                                  | 11        | 23.91%          |
| **Total Team Members**                      | **46**    | **100%**        |

| **Race/Ethnicity (Global)**                 | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 1         | 0.10%           |
| Asian                                       | 81        | 8.13%           |
| Black or African American                   | 26        | 2.61%           |
| Hispanic or Latino                          | 50        | 5.02%           |
| Native Hawaiian or Other Pacific Islander   | 0         | 0.00%           |
| Two or More Races                           | 30        | 3.01%           |
| White                                       | 547       | 54.92%          |
| Unreported                                  | 261       | 26.20%          |
| **Total Team Members**                      | **996**   | **100%**        |

| **Race/Ethnicity in Development (Global)**  | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 0         | 0.00%           |
| Asian                                       | 39        | 9.20%           |
| Black or African American                   | 7         | 1.65%           |
| Hispanic or Latino                          | 25        | 5.90%           |
| Native Hawaiian or Other Pacific Islander   | 0         | 0.00%           |
| Two or More Races                           | 12        | 2.83%           |
| White                                       | 238       | 56.13%          |
| Unreported                                  | 103       | 24.29%          |
| **Total Team Members**                      | **424**   | **100%**        |

| **Race/Ethnicity in Leadership (Global)**   | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 0         | 0.00%           |
| Asian                                       | 6         | 10.53%          |
| Black or African American                   | 0         | 0.00%           |
| Hispanic or Latino                          | 1         | 1.75%           |
| Native Hawaiian or Other Pacific Islander   | 0         | 0.00%           |
| Two or More Races                           | 2         | 3.51%           |
| White                                       | 32        | 56.14%          |
| Unreported                                  | 16        | 28.07%          |
| **Total Team Members**                      | **57**    | **100%**        |

##### Age Distribution

| **Age Distribution (Global)**               | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| 18-24                                       | 18        | 1.81%           |
| 25-29                                       | 187       | 18.78%          |
| 30-34                                       | 286       | 28.71%          |
| 35-39                                       | 196       | 19.68%          |
| 40-49                                       | 205       | 20.58%          |
| 50-59                                       | 94        | 9.44%           |
| 60+                                         | 9         | 0.90%           |
| Unreported                                  | 1         | 0.10%           |
| **Total Team Members**                      | **996**   | **100%**        |


Source: GitLab's HRIS, BambooHR
