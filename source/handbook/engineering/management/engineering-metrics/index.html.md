---
layout: handbook-page-toc
title: "Engineering Metrics"
---

{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

There are many ways to develop engineering metrics. Ideally you need a representation of different aspects of the
operational data such as throughput, quality and cycle time to name a few.

The goal of gathering this data is to define what a healthy and successful team looks like and to foster a healthy dialogue amongst the engineering team and with stakeholders.
These metrics are helpful for conversations around planning, addressing technical debt, capacity to work on bugs,
identifying bottlenecks and improving engineering efficiency. Metrics are a good way to also see how change affects the team’s execution. 

Engineering metrics are project or team based and are not intended to track an individual's capacity or performance.
It is important to note that if metrics are used punitively, these goals are hampered and the team psychological safety could be at risk.

As a manager you should ensure you are on top of your team's metrics, driving the right behavior, and understanding any fluctuations. Expect to be able to discuss your team's metrics with your manager on a weekly basis.

To access our current dashboard, please visit the [GitLab Insight Quality Page](https://quality-dashboard.gitlap.com/groups/gitlab-org).
We are currently working to implement these graphs into the GitLab product.

## List of Metrics

- [Throughput](/handbook/engineering/management/throughput/): This is our measure for number of MRs merged in a defined time frame (week and months).
  The current calculations includes merges in all branches and not just `master`.
  - [Engineering throughput](https://quality-dashboard.gitlap.com/groups/gitlab-org/throughputs):
    This chart shows the throughput at the `gitlab-org` group level.
  - [Group throughputs (using Plan:Portfolio Management as an example)](https://quality-dashboard.gitlap.com/groups/gitlab-org/sections/group::portfolio_management):
    The throughput graph is also available for each group.
  - Data for throughput is available at the weekly and monthly level. We show 12 weeks for weekly and 24 months for monthly views.
- [Average MRs merged per author per Month](https://quality-dashboard.gitlap.com/groups/gitlab-org/average_mrs_per_month):
  This is our measure calculated by the total number of MRs merged in a given month divided by the total unique authors who created those MRs.
  We currently get all the unique authors from that period and not just filtered to engineering.
- [Bugs by Priority (monthly)](https://quality-dashboard.gitlap.com/groups/gitlab-org/bugs_by_priority): This chart shows the monthly bugs by priority over time.
- [Bugs by Severity (monthly)](https://quality-dashboard.gitlap.com/groups/gitlab-org/bugs_by_severity): This chart shows the monthly bugs by severity over time.
- [Bug Classification](https://quality-dashboard.gitlap.com/groups/gitlab-org/bug_classification): This chart shows the overall of priority and severity of current open bugs.
- [Regressions per Milestone](https://quality-dashboard.gitlap.com/groups/gitlab-org/regressions): This chart shows the number of regressions found in a release.
  - **Known issues** Data here is dependent on the `regression:x.y` label. It is possible that some regressions are not captured due to it not having a regression label.
- [Weekly Pulse Survey](/handbook/engineering/management/pulse-survey/): We are working to roll out a weekly pulse survey across all of the engineering teams.
  The goal is to provide a weekly set of data of how the individuals on the team feel in regards to 3 main areas:
  Their work, their manager, and GitLab.
  In addition to the numeric responses, a comment field is available.
  This survey is anonymous and the comments in the results are only visible by the director administering the survey.

## Data included in calculation

### Engineering wide projects

We are currently working on including all of engineering's projects. The following projects included can be seen in this [list](https://quality-dashboard.gitlap.com/groups/gitlab-org/projects).

### Security fixes on dev instance

We also capture data from our `https://dev.gitlab.org/` instance which currently includes the projects below.

- gitlabhq (CE)
- gitlab-ee
- gitaly
- gitlab-shell
- gitlab-pages
- GitLab runner

The data from dev instance is only reflected in a few group level charts, see list of known issues below.

### Known issues

- Due to the lack of team labels on `https://dev.gitlab.org/`, only the 3 group-level charts below are reflected with data from this source.
  - [Engineering throughput](https://quality-dashboard.gitlap.com/groups/gitlab-org/throughputs)
  - [Average MRs merged per engineer per Month](https://quality-dashboard.gitlap.com/groups/gitlab-org/average_mrs_per_month)
  - [Average MRs merged per engineer per Milestone](https://quality-dashboard.gitlap.com/groups/gitlab-org/average_mrs_per_milestone)
- Insufficient labelled data will be listed under `undefined` category.
- A small amount of Merge Requests are not visualized in the charts due to missing `merged_at` attribute.
  - Bug filed: [API: `merged_at` fields missing for certain Merge Requests in `merged` state](https://gitlab.com/gitlab-org/gitlab/issues/26911)

### Adding more metrics

If you or your team needs to include a new project in the metrics or add a new team, please create an issue in the [GitLab Insights issue tracker](https://gitlab.com/gitlab-org/gitlab-insights/issues).

We have a list of projects that are still not included in this [issue](https://gitlab.com/gitlab-org/gitlab-insights/issues/56#projects-not-introduced-yet).

## Custom team metrics dashboard in Periscope

Considering how one of our [development department KPIs](/handbook/business-ops/data-team/metrics/#development-department-kpis) is to have a goal of 8 MRs per Engineer per Month, here are some steps on how to build your own custom team metrics dashboard on Periscope so that managers can better coach their teams to achieve the KPI target.

> Note: These dashboards will be available internally to everyone at GitLab (as per default periscope permissions). Considering that the majority of engineer MR activity is public, we do not anticipate this being an issue. In addition, anyone that can view the dashboard will also be able to toggle the filter to see individual team member metrics. We purposefully do not chart all team members in a specific chart against each other because throughput is just one data point and is not a holistic evaluation of a team member's productivity/performance.

> Note: MRs that are merged in dev.gitlab.org are not available in periscope: [gitlab-data/analytics#2789](https://gitlab.com/gitlab-data/analytics/issues/2789)

### Instructions

1. Make sure you have editor access on periscope. If not, please [create an access request](/handbook/business-ops/data-team/periscope/#accessing-periscope) for one.
1. In periscope, open up the [Frontend Monitor:Health Development Metrics](https://app.periscopedata.com/app/gitlab/527783/WIP:-Frontend-Monitor:Health-Development-Metrics) dashboard
1. [Clone the periscope dashboard](https://doc.periscopedata.com/article/dashboard-overview#Clone)
1. Rename the dashboard by clicking on the title at the top
1. Below the title, there should be a filters dropdown, click on it to expand the filters
1. Click on the `+` icon on the right to create a new filter
1. Name your filter based on your team. Note: These filters are not scoped to your dashboard. Other dashboards can use your filter, keep that in mind when naming.
1. Click the radio button that says `Type in names and values`
1. Type in the name of your team member (this is just for visual reference, it does not impact the result) and the team member's GitLab user id as the value. To find the user id, you can go to this URL in your browser: `https://gitlab.com/api/v4/users?username=USERNAME`. Replace `USERNAME` with your team member's GitLab username.
1. Click Save
1. Check `Select Displayed` under your newly created filter to filter the results by your entire team
1. Click the `edit` button in every chart on the dashboard and replace every reference of `frontend_monitor_health_team` with the name of your filter so that the dashboard is filtered by your results
1. Please also update the `What am I looking at?` section so that it accurately reflects your team information
