---
layout: handbook-page-toc
title: "Global Volunteer Month"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

### Global Volunteer Month
This initiative is aimed at volunteering during the month of December each year, within your local community. 

How did it work in previous years?

1. Join `#GiveLab` in Slack by mid-November of that year and start searching for a local initiative you can support. 

2. This initiative include's any form of volunteering! Some examples to help you search: volunteering to create a quick and easy website for a soup kitchen, helping a non-profit fix bugs in their app, collecting trash at your local park, serving at a soup-kitchen, putting together a small box of things for kids in underprivilegde community, reading at a local childrens-home or shelter.

3. Share your volunteering on social media and tag @gitlab and use the tag: #GiveLab\
*GitLab social handles may engage with and/or share your post.*\
*The ability to share posts is dependent on their demands at the time and is not promised.*

4. Thank you for taking part and helping us spread some love! 
